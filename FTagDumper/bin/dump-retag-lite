#!/usr/bin/env python

"""Lightweight retagging script

Runs some variations and saves the GN2 scores on jets.

This is distinct from the "full" tagging that we use in most flavor
tagging. You can run that with `dump-retag`.

The main differences here are:

- Athena is optional (required for the "rich"  track extrapolation)
- No b-tagging object is created
- No secondary vertexing code is run, this is strictly NNs

"""

from argparse import ArgumentParser
import sys

from FTagDumper.dumper import getMainConfig as getConfig

from FTagDumper import dumper
from FTagDumper import retag_lite as retag

import copy

def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    return parser.parse_args()

all_syst_sets = {'track_systematics', 'jet_systematics'}

def _reset_print_interval(flags, config_all):
    # turn down the print interval for lots of systs
    n_systematics = sum(len(config_all[x]) for x in all_syst_sets)
    old = flags.Exec.EventPrintoutInterval
    flags.Exec.EventPrintoutInterval = old // (n_systematics + 1) + 1


def run():

    args = get_args()

    flags = dumper.update_flags(args)
    config_all = dumper.combinedConfig(args.config_file)
    _reset_print_interval(flags, config_all)

    flags.lock()

    ca = getConfig(flags)

    constargs = dict(
        output=args.output,
        force_full_precision=args.force_full_precision
    )

    # add one set of algs for each track and jet systematic
    for syst_set in all_syst_sets:
        for syst in config_all[syst_set]:
            config = copy.deepcopy(config_all)
            config[syst_set] = [syst]
            for rmsys in all_syst_sets - set([syst_set]):
                config[rmsys] = []

            ca.merge(
                retag.dumpster_cfg(
                    flags,
                    config=config,
                    **constargs
                )
            )
    # now add nominal as well
    nom_config = copy.deepcopy(config_all)
    for rmsys in all_syst_sets:
        nom_config[rmsys] = []
    ca.merge(
        retag.dumpster_cfg(
            flags,
            config=nom_config,
            **constargs
        )
    )
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
