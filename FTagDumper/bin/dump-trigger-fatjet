#!/usr/bin/env python

"""
Dumper for large-R jets that are reconstructed in trigger
"""

import sys
from FTagDumper import dumper, mctc
from FTagDumper.trigger import getLabelingBuilderAlg, getJobConfig
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagConfig import BTagAlgsCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg
from ParticleJetTools.ParticleJetToolsConfig import (
    getJetDeltaRFlavorLabelTool)

def getRequisiteAlgsCA(flags):
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca = ComponentAccumulator()
    ca.merge(mctc.getMCTC())
    ca.addEventAlgo(CompFactory.ParentLinkDecoratorAlg(
            name = "ParentLinkDecoratorAlg"))
    labelAlg = getLabelingBuilderAlg(flags)
    ca.addEventAlgo(labelAlg)
    DRTool = getJetDeltaRFlavorLabelTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
        "HLT_TrackJetDeltaRLabelingAlg",
        Decorators = [DRTool],
        JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        name = "PoorMansIpAugmenter_HLT_IDTrack_FS_FTF",
        prefix = 'btagIp_',
        trackContainer = fs_tracks,
        primaryVertexContainer = fs_vertices))

    OverlapDecoTool = CompFactory.TriggerVRJetOverlapDecoratorTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
            "HLT_VRJetOverlapDecoratorAlg",
            Decorators = [OverlapDecoTool],
            JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"))
    ca.merge(JetTagCalibCfg(flags))
    ca.merge(BTagAlgsCfg(
        flags,
        JetCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        nnList=['BTagging/20211216trig/dips/AntiKt4EMPFlow/network.json'],
        trackCollection= fs_tracks,
        primaryVertices= fs_vertices,
        muons='',
        renameTrackJets = False,
        AddedJetSuffix=''
    ))
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection="HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf",
        InputParticleCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        OutputParticleDecoration="PV0TrackJets"))

    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()

    
    flags = dumper.update_flags(args)
    flags.lock()

    # load configurations
    combined_cfg = dumper.combinedConfig(args.config_file)
    trig_cfg, dumper_cfg = getJobConfig(combined_cfg)

    top_level_ca = dumper.getMainConfig(flags)
    top_level_ca.merge(getRequisiteAlgsCA(flags))
    top_level_ca.merge(dumper.getDumperConfig(args, config_dict=dumper_cfg))
    return top_level_ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
