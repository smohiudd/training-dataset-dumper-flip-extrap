from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from JetRecConfig.StandardSmallRJets import AntiKt4EMTopo
from JetRecConfig.JetRecConfig import JetRecCfg
from JetRecConfig.JetConfigFlags import jetInternalFlags

from .BaseBlock import BaseBlock


@dataclass
class JetReco(BaseBlock):
    '''Reconstructs a jet collection
    Parameters
    ----------
    Duplicate_EMTopojets : bool,
        The jet collection to be reconstructed already exists in the AOD but links are broken. 
        Need to duplicate to reconstruct jets with a new name
    '''
    Duplicate_EMTopojets: str = ''
    
    def __post_init__(self):
        if self.Duplicate_EMTopojets is None:
            self.Duplicate_EMTopojets = self.dumper_config['Duplicate_Jets']

    def to_ca(self):

        acc = ComponentAccumulator()




        if self.Duplicate_EMTopojets:
            AntiKt4EMTopov2 = AntiKt4EMTopo.clone(suffix="v2")
            
            jetInternalFlags.isRecoJob = True
            acc.merge(JetRecCfg(self.flags, AntiKt4EMTopov2))
            
        else: 
            jetInternalFlags.isRecoJob = True
            acc.merge(JetRecCfg(self.flags, AntiKt4EMTopo))
        return acc
