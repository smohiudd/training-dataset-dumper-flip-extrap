#include "DecoratorExample.hh"

#include "xAODBTagging/BTaggingUtilities.h"

// the constructor just builds the decorator
DecoratorExample::DecoratorExample(const std::string& prefix,
                                   const DecoratorExample::BLA& btag_link):
  m_deco(prefix + "decorator"),
  m_btagging_link(btag_link)
{
}

// this call actually does the work on the jet
void DecoratorExample::decorate(const xAOD::Jet& jet) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = *m_btagging_link(jet);
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }

  // Store something to the jet.
  m_deco(*btag) = std::log(jet.pt());
}
