#include "LeptonTruthDecorator.hh"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"
#include "xAODBTagging/BTagging.h"
#include "xAODTruth/TruthParticleContainer.h"

// the constructor just builds the decorator
LeptonTruthDecorator::LeptonTruthDecorator()
    : m_acc_softMuon_link("softMuon_link"),
      m_acc_softMuon_truthOrigin("truthOrigin"),
      m_acc_softMuon_truthType("truthType"),
      m_deco_softMuon_truthOrigin("softMuon_truthOrigin"),
      m_deco_softMuon_truthType("softMuon_truthType") {}

// this call actually does the work on the jet
void LeptonTruthDecorator::decorate(
    const xAOD::BTagging& btag,
    const xAOD::Jet& jet) const {

  int softMuon_truthOrigin = -1;
  int softMuon_truthType = -1;

  // check for an associated soft muon and get info
  auto softMuon_link = m_acc_softMuon_link(btag);
  if (softMuon_link.isValid()) {
    const xAOD::Muon& muon = **softMuon_link;
    softMuon_truthOrigin = m_acc_softMuon_truthOrigin(muon);
    softMuon_truthType = m_acc_softMuon_truthType(muon);
  }

  // decorate
  m_deco_softMuon_truthOrigin(jet) = softMuon_truthOrigin;
  m_deco_softMuon_truthType(jet) = softMuon_truthType;

}
