#include "TruthWriter.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

namespace {
  Amg::Vector3D v3(const xAOD::TruthVertex& p) {
    return {p.x(), p.y(), p.z()};
  }
  Amg::Vector3D d3(const TruthOutputs& out) {
    if ( out.truth->decayVtx() ) {
      return v3(*out.truth->decayVtx()) - out.origin;
    }
    else return {NAN, NAN, NAN};
  }
  Amg::Vector3D p3(const xAOD::Jet& j) {
    return {j.px(), j.py(), j.pz()};
  }
}


typedef std::function<float(const TruthOutputs&)> FloatFiller;
typedef std::function<int(const TruthOutputs&)> IntFiller;
class TruthConsumers: public H5Utils::Consumers<const TruthOutputs&> {};
class TruthOutputWriter: public H5Utils::Writer<1,const TruthOutputs&>
{
public:
  TruthOutputWriter(H5::Group& file,
                    const std::string& name,
                    const TruthConsumers& cons,
                    size_t size):
    H5Utils::Writer<1,const TruthOutputs&>(file, name, cons, {{size}}) {}
};

TruthWriter::TruthWriter(
  H5::Group& output_file,
  const std::size_t output_size,
  const std::string& link_name,
  const std::string& output_name,
  TrackSortOrder order):
  m_hdf5_truth_writer(nullptr),
  m_acc(link_name),
  m_output_size(output_size)
{
  using TO = TruthOutputs;
  const auto h = H5Utils::Compression::HALF_PRECISION;
  if ( order == TrackSortOrder::PT ) {
    m_sort = [](const xAOD::TruthParticle* p1, const xAOD::TruthParticle* p2) {
      return (p1->pt() > p2->pt());
    };
  } else {
    throw std::logic_error("undefined sort order");
  }

  TruthConsumers fillers;

  // hard coded fillers
  FloatFiller pt = [](const TruthOutputs& t) -> float {
    return t.truth->pt();
  };
  fillers.add("pt", pt, NAN);

  FloatFiller mass = [](const TruthOutputs& t) -> float {
    return t.truth->m();
  };
  fillers.add("mass", mass, NAN);

 FloatFiller e = [](const TruthOutputs& t) -> float {
    return t.truth->e();
  };
  fillers.add("energy", e, NAN);

  FloatFiller eta = [](const TruthOutputs& t) -> float {
    return t.truth->eta();
  };
  fillers.add("eta", eta, NAN, h);

  FloatFiller phi = [](const TruthOutputs& t) -> float {
    return t.truth->phi();
  };
  fillers.add("phi", phi, NAN, h);

  FloatFiller deta = [](const TruthOutputs& t) -> float {
    return t.jet->eta() - t.truth->eta();
  };
  fillers.add("deta", deta, NAN, h);

  FloatFiller dphi = [](const TruthOutputs& t) -> float {
    return t.jet->p4().DeltaPhi(t.truth->p4());
  };
  fillers.add("dphi", dphi, NAN, h);

  FloatFiller dr = [](const TruthOutputs& t) -> float {
    return t.jet->p4().DeltaR(t.truth->p4());
  };
  fillers.add("dr", dr, NAN, h);

  fillers.add(
    "Lxy",
    [](const TO& t) { return d3(t).perp(); },
    NAN, h);
  fillers.add(
    "decayVertexDPhi", [](const TO& t) {
      return p3(*t.jet).deltaPhi(d3(t));
    },
    NAN, h);
  fillers.add(
    "decayVertexDEta", [](const TO& t) {
      return d3(t).eta() - p3(*t.jet).eta();
    },
    NAN, h);

  IntFiller charge = [](const TruthOutputs& t) -> int {
    return t.truth->charge();
  };
  fillers.add("charge", charge, -2);

  IntFiller flavour = [](const TruthOutputs& t) -> int {
    if ( t.truth->isBottomHadron() ) { return 5; }
    if ( t.truth->isCharmHadron()  ) { return 4; }
    return -1;
  };
  fillers.add("flavour", flavour, -1);

  IntFiller pdgId = [](const TruthOutputs& t) -> int {
    return t.truth->pdgId();
  };
  fillers.add("pdgId", pdgId, -1);

  IntFiller barcode = [](const TruthOutputs& t) -> int {
    return t.truth->barcode();
  };
  fillers.add("barcode", barcode, -1);

  IntFiller parent_barcode = [](const TruthOutputs& t) -> int {
   return t.truth->parent() ? t.truth->parent()->barcode() : -1;
  };
  fillers.add("parentBarcode", parent_barcode, -1);

  // add valid flag, for more robust selection, true for any track
  // that is defined.
  fillers.add("valid", [](const auto&) { return true; }, false);

  // build the output dataset
  if (link_name.size() == 0) {
    throw std::logic_error("output name not specified");
  }
  if (m_output_size == 0) {
    throw std::logic_error("can't make an output writer with no truth particles!");
  }
  m_hdf5_truth_writer.reset(
    new TruthOutputWriter(
      output_file, output_name, fillers, m_output_size));
}

TruthWriter::~TruthWriter() {
  if (m_hdf5_truth_writer) m_hdf5_truth_writer->flush();
}

TruthWriter::TruthWriter(TruthWriter&&) = default;

void TruthWriter::write(const xAOD::Jet& jet, const Amg::Vector3D& origin) {
  if (m_hdf5_truth_writer) {

    // get linked truth particles
    auto truth_particles = get_truth_parts(jet);

    // get information about each truth
    std::vector<TruthOutputs> truth_outputs;
    for (const auto* truth: truth_particles) {
      truth_outputs.push_back(
        TruthOutputs{
          truth,
          &jet,
          origin
        });
    }

    // write
    m_hdf5_truth_writer->fill(truth_outputs);
  }
}
void TruthWriter::write_dummy() {
  if (m_hdf5_truth_writer) {
    std::vector<TruthOutputs> truth_outputs;
    m_hdf5_truth_writer->fill(truth_outputs);
  }
}

// access decorated particles
TruthWriter::Truths TruthWriter::get_truth_parts(const xAOD::Jet& jet) const
{
  Truths truth_particles;
  for (const auto& link: m_acc(jet)) {
    if (!link.isValid()) {
      throw std::logic_error("invalid truth link");
    }
    const xAOD::IParticle* part = *link;
    const auto* truth = dynamic_cast<const xAOD::TruthParticle*>(part);
    if (!truth) {
      throw std::runtime_error(
        "Truth writer could not cast xAOD::IParticle to xAOD::TruthParticle");
    }
    truth_particles.push_back(truth);
  }
  std::sort(truth_particles.begin(), truth_particles.end(), m_sort);
  return truth_particles;
}
