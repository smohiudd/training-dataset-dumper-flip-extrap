#include "SoftElectronSelector.hh"
#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODEgamma/Electron.h"


SoftElectronSelector::SoftElectronSelector(SoftElectronSelectorConfig config):
  m_electron_select_cfg(config.cuts)
{
}


SoftElectronSelector::Electrons SoftElectronSelector::get_electrons(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons) const
{
  SoftElectronSelector::Electrons selected_electrons;
  for (const auto& el : electrons) {
    if (passed_cuts(jet, *el)) {
      selected_electrons.push_back(el);
    }
  }
  return selected_electrons;
}


bool SoftElectronSelector::passed_cuts(const xAOD::Jet& jet, const xAOD::Electron& el) const
{
  TLorentzVector jet_4vec;
  TLorentzVector el_4vec;

  static SG::AuxElement::ConstAccessor<float> m_dpop("ftag_deltaPOverP");
  static SG::AuxElement::ConstAccessor<float> m_iso_pt("ftag_ptVarCone30OverPt");

  jet_4vec.SetPtEtaPhiE(jet.pt(), jet.eta(), jet.phi(), jet.e());
  el_4vec.SetPtEtaPhiE(el.pt(), el.eta(), el.phi(), el.e());
  if (jet_4vec.DeltaR(el_4vec) > 0.4)
    return false;
  float el_ptrel = el_4vec.Vect().Perp(jet_4vec.Vect());

  // Get shower shapes
  float el_rhad1 = el.showerShapeValue(xAOD::EgammaParameters::Rhad1);
  float el_wstot = el.showerShapeValue(xAOD::EgammaParameters::wtots1);
  float el_rphi  = el.showerShapeValue(xAOD::EgammaParameters::Rphi);
  float el_reta  = el.showerShapeValue(xAOD::EgammaParameters::Reta);
  float el_deta1 = el.trackCaloMatchValue(xAOD::EgammaParameters::deltaEta1);

  if (std::abs(el.eta()) > m_electron_select_cfg.abs_eta_maximum)
    return false;
  if (el.pt() <= m_electron_select_cfg.pt_minimum)
    return false;
  if (el.pt() >= m_electron_select_cfg.pt_maximum)
    return false;

  auto track = el.trackParticle();

  if (std::isfinite(m_electron_select_cfg.d0_maximum) &&
      std::abs(track->d0()) >= m_electron_select_cfg.d0_maximum)
    return false;
  if (std::abs(el.caloCluster()->e() * track->qOverP()) > m_electron_select_cfg.eop_maximum)
    return false;
  if (std::abs(el.caloCluster()->e() / std::cosh(track->eta())) > m_electron_select_cfg.eta_maximum)
    return false;
  if (std::abs(m_iso_pt(el)) > m_electron_select_cfg.isopt_maximum)
    return false;
  if (std::abs(el_ptrel) > m_electron_select_cfg.ptrel_maximum)
    return false;
  if (std::abs(el_rhad1) > m_electron_select_cfg.rhad1_maximum)
    return false;
  if (std::abs(el_wstot) > m_electron_select_cfg.wstot_maximum)
    return false;
  if (std::abs(el_rphi) > m_electron_select_cfg.rphi_maximum)
    return false;
  if (std::abs(el_reta) > m_electron_select_cfg.reta_maximum)
    return false;
  if (std::abs(el_deta1) > m_electron_select_cfg.deta1_maximum)
    return false;
  if (std::abs(m_dpop(el)) > m_electron_select_cfg.dpop_maximum)
    return false;
  return true;
}



