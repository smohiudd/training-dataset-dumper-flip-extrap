#ifndef GHOST_ASSOCIATION_SYST_ALG_HH
#define GHOST_ASSOCIATION_SYST_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODJet/JetContainer.h"
#include "AthLinks/ElementLink.h"

class GhostAssociationSystAlg :  public AthReentrantAlgorithm {
public:

  /**< Constructors */
  GhostAssociationSystAlg(const std::string& name, ISvcLocator *pSvcLocator);

  /**< Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&)  const override;

private:

  using IPC = xAOD::IParticleContainer;
  using IPLV = std::vector<ElementLink<xAOD::IParticleContainer>>;
  using IPL = ElementLink< xAOD::IParticleContainer >;
  
  bool getCorrectedLinks;

  SG::ReadHandleKey<IPC> m_jet_collection {
    this, "JetCollection", "Jets",
      "Key for the input jets"};

  SG::ReadDecorHandleKey<IPC> m_tracksIn {
    this, "InGhostTracks", "Jets.In",
    "Original tracks"};

  SG::WriteDecorHandleKey<IPC> m_tracksOut {
    this, "OutGhostTracks", "Jets.Out",
    "Link to be added to the Jet for syst tracks"};

};

#endif
