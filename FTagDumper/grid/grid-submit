#!/usr/bin/env bash

# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi

BREAK="----------------------------------------------"
###################################################
# Add some mode switches
###################################################
declare -A SCRIPTS_BY_MODE=(
    [single-btag]=dump-single-btag
    [retag]=dump-retag
    [retag-fatjet]=dump-retag-fatjet
    [retag-extrapolation]=dump-retag-lite
    [generator-weights]=dump-single-btag
    [alternative-generators]=dump-single-btag
    [trigger]=dump-trigger-pflow
    [trigger-emtopo]=dump-trigger-emtopo
    [trigger-hits]=dump-trigger-emtopo
    [trigger-wp]=dump-trigger-workingpoints
    [trigger-all]=dump-trigger-all
    [trigger-val]=dump-trigger-all
    [trigger-trackjet]=dump-trigger-trackjet
    [upgrade]=dump-single-btag
    [upgrade-HI]=dump-upgrade-HI
    [nntc]=dump-fixedcone
    [xbb]=dump-fatjet
    [trackless]=dump-single-btag
    [taucomp]=dump-single-btag
    [smeared-tracks]=dump-smeared-tracks
    [CaloHits]=dump-single-btag

)
declare -A CONFIGS_BY_MODE=(
    [single-btag]=EMPFlow.json
    [retag]=EMPFlowRetagAthena.json
    [retag-fatjet]=FatJetsRetag.json
    [retag-extrapolation]=EMPFlowRetagAthena.json
    [generator-weights]=EMPFlowGeneratorSystematics.json
    [alternative-generators]=EMPFlow.json
    [trigger]=trigger_pflow.json
    [trigger-emtopo]=trigger_emtopo.json
    [trigger-hits]=trigger_hits.json
    [trigger-wp]=trigger_wp.json
    [trigger-all]=trigger_all.json
    [trigger-val]=trigger_all_SampleA.json
    [trigger-trackjet]=TriggerTrackJets.json
    [upgrade]=upgrade_r24.json
    [upgrade-HI]=upgrade-HI.json
    [nntc]=EMPFlow_fixedcone.json
    [xbb]=FatJetsFlow.json
    [trackless]=TracklessEMPFlow.json
    [taucomp]=TauComp.json
    [smeared-tracks]=upgrade_r24.json
    [CaloHits]=EMTopo_CaloclustersHits.json

)
declare -A INPUTS_BY_MODE=(
    [single-btag]=single-btag.txt
    [retag]=retag-vary-systs.txt
    [retag-fatjets]=retag-fatjet.txt
    [retag-extrapolation]=retag-extrapolation.txt
    [generator-weights]=generator-weights.txt
    [alternative-generators]=alternative-generators.txt
    [trigger]=trigger.txt
    [trigger-emtopo]=trigger-emtopo.txt
    [trigger-hits]=trigger-hits.txt
    [trigger-wp]=trigger-workingpoints.txt
    [trigger-all]=trigger.txt
    [trigger-val]=trigger-val.txt
    [trigger-trackjet]=trigger-trackjet.txt
    [upgrade]=upgrade.txt
    [upgrade-HI]=upgrade-HI.txt
    [nntc]=single-btag.txt
    [xbb]=xbb.txt
    [trackless]=trackless.txt
    [taucomp]=daod_phys.txt
    [smeared-tracks]=upgrade.txt
    [CaloHits]=trigger-hits.txt

)
###################################################
# CLI
###################################################
_usage() {
    echo "usage: ${0##*/} [-h] [options] MODE"
}
_help() {
    _usage
    cat <<EOF

    Submit dumper jobs to the grid! This script will ask you to commit your changes.
    Specify a running MODE (below) and then optionally overwrite the mode's defaults
    using the optional flags. You are encoraged to use the -t argument to tag your
    submission.

Options:
 -s script : Executable to be run (e.g. dump-single-btag)
 -c config : Path to the config file to use for the jobs
 -i file   : File listing input samples, will override the default list
 -t tag    : Tag the current code state using the supplied string
 -f        : Force, don't require changes to be committed
 -m        : Use --forceStage prun flag
 -n        : Number of events to process per input sample
 -a        : Run a test job using only one file per dataset
 -d        : Dry run, don't submit anything or make a tarball, but build the
             submit directory
 -e        : external files to be added to grid job

Modes:
$(for key in "${!CONFIGS_BY_MODE[@]}"; do
    echo -e " $key   \t=> ${SCRIPTS_BY_MODE[$key]} -c ${CONFIGS_BY_MODE[$key]}";
done)

EOF
}

# defaults
SCRIPT=""; CONFIG=""; INPUT_DATASETS=""; EXT_FILES="";
TAG=""; FORCE=""; DRYRUN=""; EXTRA_ARGS=""; ANNOTATE="";
NUM_EVENTS=-1
while getopts ":hs:c:i:t:e:n:fmad" opt $@;
do
    case $opt in
        h) _help; exit 1;;
        s) SCRIPT=${OPTARG};;
        c) CONFIG=${OPTARG};;
        i) INPUT_DATASETS=${OPTARG};;
        t) TAG=${OPTARG};;
        e) EXT_FILES=${OPTARG};;
        n) NUM_EVENTS=${OPTARG};;
        f) FORCE=1 ;;
        m) EXTRA_ARGS+=' --forceStaged ';;
        a) EXTRA_ARGS+=' --nFiles 1 '; ANNOTATE+=.test ;;
        d) DRYRUN="echo DRY RUNNING: " ;;
        # handle errors
        \?) _usage; echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :)  _usage; echo "Missing argument for -$OPTARG" >&2; exit 1;;
        *)  _usage; echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done
shift $((OPTIND-1))

# check required args
if [[ "$#" -ne 1 ]]; then
    echo "ERROR: Please specify a running mode after optional arguments" >&2
    _usage
    exit 1
fi
MODE=$1

if [[ -z ${SCRIPTS_BY_MODE[$MODE]+foo} ]]; then
    echo "ERROR: Invalid mode! Run ${0##*/} -h for allowed modes" >&2
    exit 1
fi

if [[ -n ${FORCE} && -n ${TAG} ]]; then
    echo "ERROR: Invalid args! Using -t with -f is not supported." >&2
    exit 1
fi

# this is where all the source files are
BASE=$(realpath --relative-to=$PWD $(dirname $(readlink -e ${BASH_SOURCE[0]}))/../..)
CONFIG_DIR=${BASE}/configs
INPUTS_DIR=${BASE}/FTagDumper/grid/inputs
WORK_DIR=$PWD

# if arguments are not specified, use mode
if [[ -z "$SCRIPT" ]]; then SCRIPT=${SCRIPTS_BY_MODE[$MODE]}; fi
if [[ -z "$CONFIG" ]]; then CONFIG=$CONFIG_DIR/${CONFIGS_BY_MODE[$MODE]}; fi
if [[ -z "$INPUT_DATASETS" ]]; then INPUT_DATASETS=$INPUTS_DIR/${INPUTS_BY_MODE[$MODE]}; fi
if [[ "$DRYRUN" ]]; then echo -e $BREAK '\nDRY RUNNING'; fi

# let the user know what options we are using
echo $BREAK
echo -e "Script:\t $SCRIPT"
echo -e "Config:\t $CONFIG"
echo -e "Inputs:\t $INPUT_DATASETS"
if [ -n "${EXT_FILES}" ]; then
    echo -e "External files:\t $EXT_FILES"
fi
echo $BREAK

###################################################
# Check for early exit
###################################################

# check arguments
if [[ ! -f $CONFIG ]]; then echo "Config file doesn't exist!"; exit 1; fi
if [[ ! -f $INPUT_DATASETS ]]; then echo "Inputs file doesn't exist!"; exit 1; fi

# check for panda setup
if ! type prun &> /dev/null ; then
    echo "ERROR: You need to source the grid setup script before continuing!" >&2
    exit 1
fi

# check to make sure you've properly set up the environemnt: if you
# haven't sourced the setup script in the build directory the grid
# submission will fail, so we check here before doing any work.
if ! type $SCRIPT &> /dev/null ; then
    echo "ERROR: Code setup with the wrong release or you haven't sourced build/x*/setup.sh" >&2
    exit 1
fi

# check for uncontrolled changes
if [[ ! $FORCE ]]; then
    if ! git -C ${BASE} diff-index --quiet HEAD; then
        echo "ERROR: uncommitted changes, please commit them" >&2; exit 1
    fi
fi

###################################################
# Some variable definitions
###################################################
# users's grid name
GRID_NAME=${RUCIO_ACCOUNT-${USER}}

######################################################
# Create an identifier for this job
######################################################
if [[ ${TAG} ]]; then
    cd ${BASE}

    ORIGIN_URL=$(git remote get-url origin)
    if [[ $ORIGIN_URL != *"$GRID_NAME"* ]]; then
        echo "ERROR: Remote URL for \"origin\" is not a fork, please submit from a fork" >&2
        exit 1
    fi

    DUMP_ID=$(git tag --points-at)
    if [[ "${DUMP_ID}" = "${TAG}" ]]; then
        echo "Found and resusing tag ${DUMP_ID}"
    else
        DUMP_ID=$(date +%y-%m-%d)_${TAG}
        echo "No tag found, creating tag ${DUMP_ID}"
        ${DRYRUN} git tag ${DUMP_ID} -m "automated tdd submission tag $(date +%F-T%H:%M:%S)"
    fi
    cd ${WORK_DIR}
elif git -C ${BASE} diff-index --quiet HEAD; then
    DUMP_ID=$(git -C ${BASE} describe)
    echo "You didn't tag the code :(, consider using -t next time"
else
    echo "You are forcing a submission with uncontrolled changes! Using datestamp as job ID"
    DUMP_ID=$(date +%y-%m-%d-T%H%M%S)
fi

######################################################
# Prep the submit area
######################################################
# this is the subdirectory we submit from
SUBMIT_DIR=submit

echo "Preparing submit directory"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "Removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi

mkdir ${SUBMIT_DIR}

# write the expanded config file to the submit dir
test-config-merge $CONFIG > ${SUBMIT_DIR%/}/${CONFIG##*/}

if [ -n "${EXT_FILES}" ]; then
    # copying all files as external files
    IFS=',' read -r -a EXT_FILES_ARR <<< "${EXT_FILES}"

    for element in "${EXT_FILES_ARR[@]}"
    do
    cp -r "${element}" "${SUBMIT_DIR}/${element}"
    done
fi

cd ${SUBMIT_DIR}
# build a zip of the files we're going to submit
ZIP=job.tgz
echo "Making tarball of local files: ${ZIP}" >&2

# the --outTarBall, --noSubmit, and --useAthenaPackages arguments are
# important. The --outDS and --exec don't matter at all here, they are
# just placeholders to keep panda from complianing.
PRUN_ARGS="--outTarBall=${ZIP} --noSubmit --useAthenaPackages \
--exec='ls' --outDS=user.${GRID_NAME}.x"

# check if ${EXT_FILES} is not empty and append it to the default args
if [ -n "${EXT_FILES}" ]; then
    PRUN_ARGS="${PRUN_ARGS} --extFile=${EXT_FILES}"
fi

# now run the script with the prun args
${DRYRUN} prun ${PRUN_ARGS}

######################################################
# Loop over datasets and submit
######################################################
# parse inputs file
INPUT_DATASETS=$(grep -v '^#'  ${WORK_DIR}/$INPUT_DATASETS)
INPUT_DATASETS=($INPUT_DATASETS)


# Get the number of files to submit for each container. If no 'n' events are selected, skip
declare -A FILES_TO_SUBMIT
if [ ! "$NUM_EVENTS" -eq -1 ]; then
    # check for rucio setup
    if ! type rucio &> /dev/null ; then
        echo "ERROR: You need to setup rucio to dump a sub-set of a dataset!" >&2
        exit 0
    fi
    while IFS=':' read -r key value; do
        FILES_TO_SUBMIT["$key"]="$value"
    done < <(get-submit-files "$NUM_EVENTS" "${INPUT_DATASETS[@]}")
fi

# loop over all inputs
echo $BREAK
$DRYRUN
echo "Submitting ${#INPUT_DATASETS[*]} datasets as ${GRID_NAME}..." >&2
echo $BREAK


# define a fucntion to do all the submitting
function submit-job() (
    set -eu
    DS=$1
    NFILES=$2

    # this regex extracts the DSID from the input dataset name, so
    # that we can give the output dataset a unique name. It's not
    # pretty: ideally we'd just suffix our input dataset name with
    # another tag. But thanks to insanely long job options names we
    # use in the generation stage we're running out of space for
    # everything else.
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})

    # build the full output dataset name
    CONFIG_FILE=${CONFIG##*/}

    # this terrible regex extracts the atlas tags, e.g. e4342_s3443...
    ATLAS_TAGS=$(egrep -o '(_?[esdfarp][0-9]{3,6}){3,}' <<< ${DS})
    TAGS=${ATLAS_TAGS}.tdd.${CONFIG_FILE%.*}
    if [[ -n $AtlasVersion ]]; then
        TAGS=${TAGS}.${AtlasVersion//./_}
    else
        TAGS=${TAGS}.${AtlasBuildStamp}
    fi

    # The full grid-job name
    OUT_DS=user.${GRID_NAME}.${DSID}.${TAGS}.${DUMP_ID}${ANNOTATE}

    # check to make sure the grid name isn't too long
    if [[ $(wc -c <<< ${OUT_DS}) -ge 120 ]] ; then
        echo "ERROR: dataset name ${OUT_DS} is too long, can't submit!" 1>&2
        return 1
    fi

    NFILES_ARG=""
    NFILES_STR=""

    if  ((NFILES > 0)); then
        NFILES_ARG=" --nFiles ${NFILES}"
        NFILES_STR="\n\t-> (${NFILES} files)"
    fi
    
    # now submit 
    printf "${DS} \n\t-> ${OUT_DS} ${NFILES_STR}\n"

    ${DRYRUN} prun --exec "${SCRIPT} %IN -c ${CONFIG_FILE}"\
        --outDS ${OUT_DS} --inDS ${DS}\
        --useAthenaPackages --inTarBall=${ZIP}\
        --mergeScript="hdf5-merge-nolock -o %OUT -i %IN"\
        --outputs output.h5\
        ${NFILES_ARG} \
        --noEmail \
        ${EXTRA_ARGS} > ${OUT_DS}.log 2>&1
    
)

# we have to export some environment variables so xargs can read them
export -f submit-job
export CONFIG GRID_NAME DUMP_ID ZIP AtlasBuildStamp SCRIPT DRYRUN EXTRA_ARGS ANNOTATE EXT_FILES

# Submit, if no request for number of events, submit all files
for key in "${INPUT_DATASETS[@]}"; do
    value="${FILES_TO_SUBMIT[$key]--1}"
    printf "%s:%s\n" "$key" "$value"
done | xargs -P 10 -I {} bash -c 'submit-job ${1%:*} ${1#*:}' bash {}

echo $BREAK
echo "Submission successful"

######################################################
# Push tags if necessary
######################################################
if [[ ${TAG} ]]; then
    echo "Pushing tag ${DUMP_ID}"
    cd ../${BASE}
    ORIGIN_URL=$(git remote get-url origin)
    if [[ $ORIGIN_URL != *"$GRID_NAME"* ]]; then
        echo "ERROR: Remote URL for \"origin\" is not a fork, please submit from a fork" >&2
        exit 1
    fi
    ${DRYRUN} git push -q origin ${DUMP_ID}
    cd ${WORK_DIR}
fi
echo $BREAK
