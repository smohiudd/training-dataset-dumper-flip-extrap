SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
source ${SCRIPT_PATH%/*}/deconda.sh

echo "=== running setupATLAS ==="
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# If you need to run on a nightly, see ATLINFR-4697
echo "=== running asetup ==="
asetup Athena,25.0.10

# add h5ls
if [[ $SCRIPT_PATH =~ / ]] ; then
    source ${SCRIPT_PATH%/*}/add-h5-tools.sh
else
    source setup/add-h5-tools.sh
fi

# make the job fail on flake8 warnings.
export FLAKE8_ATLAS_ERROR=1
source ${SCRIPT_PATH%/*}/allow-breaking-edm.sh
export PATHRESOLVER_DEVAREARESPONSE=INFO
